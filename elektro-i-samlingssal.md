# Erfaringer med å holde elektroavdeling i samlingssalen

Samlingssalen har desidert størst arealer, forutsatt at man kan bruke benkene for å stille ut varer.

I tillegg til benkene har vi pleid å kjøre en halvsirkel med klaffbord nede på gulvet, med publikum på utsiden av klaffbordene, og av og til utstillinger av varer med høyere verdi på innsiden.  Ett år kjørte vi også full sirkel på klaffbordene, og prøvde å bruke gardinene for å stenge av baksiden av sirkelen under rushtiden.  Det fungerte dårlig.

Strømkontakter for testing av varer på søylene, ved nødutgangen øverst, samt med skjøteledninger langs utstillingsbordene.

Lokasjon for taklampeutstilling har variert litt, men jeg har etterhvert funnet ut at det er best å ta det langs enden av benkeradene helt til høyre i samlingssalen.

## Benker

Vi har pleid å ha lamper til høyre i samlingssalen, kjøkkenelektrisk helt til venstre, og diverse til venstre for midtgang.  Kabler, strømforsyninger o.l. på nest høyeste benkerad, og "søppel" og reservedeler på høyeste benkerad.

Første benkerad har vi strebet etter å holde tom, av flere årsaker:

* Det kan være fordelaktig å kunne bruke benken til å sitte på
* Det er enda vanskeligere å kontrollere første benkerad enn de andre benkeradene når det er mye folk i samlingssalen.
* I utplasseringsfasen er det veldig vanlig at folk lemner bokser og gjenstander på første benkerad, da blir det fort rot dersom man prøver å ordne fin utstilling der.

Benkeradene har noen utfordringer:

* De er ikke flate, de er beregnet til å sitte på ikke til å utstille varer - så det er ikke alt som kan stilles ut på dem.
* Det er vanskelig å kontrollere utstillingen, og vi har en del "problemkunder" som lager rot i utstillingen, samt tjuvpakk som rett og slett stjeler de mer attraktive loppene

## Klaffbordene

Jeg tror vi har brukt omtrent 11 klaffbord.

Halvsirkelen har også hatt utfordringer.  Klaffbordene er også utsatt for tyverier og rot dersom det er for mange kunder ifht vakter.

Dette har fungert halvbra, vi har hatt utfordringer med at kunder har rotet seg inn på innsiden av klaffbordene.  Kanskje man inviterer én person som har interesse for å se nærmere på noen av de utstilte varene inn, det oppfattes lett som "fritt frem" for andre kunder.  Det har også vært vanskelig å komme seg inn og ut for vakter, og det har vært en del behov for dette med så mange varer på utsiden av bordene (og med kabler og strømforsyninger helt øverst).

## Taklampeutstillingen

Jeg mener det er en stor fordel å henge opp taklamper, for å gi dem en fullverdig presentasjon.

Det har vært utfordrende å finne måter å få hengt opp taklampene på, jeg har etterhvert funnet noen muligheter, men det forutsetter bruk av mye hyssing, den må strammes veldig godt, og den bør ha god bruddstyrke slik at man slipper å bekymre seg når man henger opp tunge lamper.  Det er i det hele tatt mye styr å henge opp lampene, ofte har jeg undret meg over om det er verd arbeidsinnsatsen mtp hvor lite taklamper som blir solgt.

## Publikumskontroll

Vi har praktisert inngang nede og utgang oppe, dette fordrer to vakter bare til å håndtere menneskestrømmen.  Disse vaktene kan til en viss grad også gjøre salgsoppgaver.

## Sikker sone

Mellomgangen bak samlingssalen (med melkeskap og HC-toalett) har vært benyttet som et trygt sted å lagre egne eiendeler, enkelte ting som allerede er solgt men ikke avhentet (særlig ting kjøpt av korpsmedlemmer/foresatte) og andre ting som ikke skal selges eller stjeles.
