# Erfaringer med å holde elektroavdeling i klasserommet sønnafor husgeråd

Dette klasserommet ble benyttet før elektro ble flyttet ned på samlingssalen.  Det ble også benyttet etter at bokavdelingen tok over samlingssalen (oktober 2018).  Det er mye mindre plass i klasserommet enn på samlingssalen.

Jeg tror vi i tidligere år har hatt pulter langsmed veggene, oss bak pultene og publikum i midten av klasserommet.  Noen ting har også fått gulvplass på utsiden av pultene.  Oktober 2018 ble pulter og bord arrangert i en M-form, samt to store klappbord med billigvarer under taklamper, i publikumsdelen.  Jeg mener det var en god måte å organisere klasserommet på.  Til neste loppemarked skal vi prøve å ha litt mer plass bak vindusrekka og stoler stående inntil vindusrekka, slik at vi kan ha mer varer bak disken (særlig foodprocessors med mange deler).

Det er mange strømkontakter langs vindusraden - men det er litt klønete å dra ledninger over til salgsdisken for å teste utstyr der.  Man bør vurdere utstrakt bruk av skjøteledninger for å få strøm tilgjengelig langs bordene.  Det er noen få strømkontakter tilgjengelig på de øvrige veggene.

## Plassproblemer

Generelt er det et problem at klasserommet fort blir for lite.  Oktober 2018 var det ikke noe stort problem da vi hadde lite lopper.  En ting vi delvis har praktisert tidligere er å flytte noe av salget ut på gangen.  Vi har forsøkt å selge lampeskjermer langs garderobeplassen, men det blir litt knot dersom lampeskjermer og lamper er på to forskjellige steder.  Mai 2019 flyttet vi lampeavdelingen ut på gangen (med unntak av særlig ekslusive lamper og taklamper), og hadde en person til å både passe på døra og lampene.  Det gikk bra fordi det ikke var for stor pågang av kunder.  En dørvakt og ett barn kunne også fungert.  Lampene utgjør svært mye areal sammenlignet med inntjeningspotensialet, og vi taper ikke nevneverdig på svinn (da vi generelt får fler lamper inn enn hva det går ut og oftest ender opp med å kaste mange lamper).  Forutsetningene er at vi har noen (f.eks. et par barn, eller dørvakten) til å holde et øye med utstillingen, samt ta betalt for ting (det er ikke alle barna som er like pålitelige).  Mai 2019 hadde vi bare én person, som av og til var fraværende (lunch, dobesøk, etc), dette medførte at enkelte kunder trodde lampene var gratis.

## Varelossing

Traller trilles ikke inn på klasserommet, men tømmes på utsiden.  Det er tidvis et problem at folk som kommer med nye varer lemper det ukritisk på utstillingsbordene slik at det blir rot.  Det er kanskje en idé å markere en "DROPZONE - USORTERT ELEKTRO".

## Utstillinger bak pultene

Det er en god idé å ha utstillinger av gjenstander med høy svinnrisiko (pga høy verdi eller liten størrelse) bak disken, slik at det ikke er umiddelbart tilgjengelig for kundene.

Oktober 2018 plasserte vi lampeskjermer høyt oppe bak disken på høyre side av klasserommet.  Dette var ikke helt optimalt da lampeskjermene ble lite tilgjengelige og lite synlige, men det er ikke stort annet dette arealet kan brukes til.

På venstre side av klasserommet er det vinduskarm - denne er litt "klønete" for å stable opp ting da karmen var skadet og hadde en helningsvinkel ut fra vinduet, og dessuten ganske smal, men de tingene som ble stående der var godt synlige.  Mai 2019 ble det brukt noe papir/papp for å lage en kant, pluss at det ble lagt noe materiale med mye friksjon langs karmen.  "Sticky pads" eller kanskje lærertyggis bør vurderes.  En ledning langs kanten, festet med vevtape, fungerer fint.

## Taklampeutstilling

De rektangulære taklampene som allerede henger i klasserommet er relativt tunge - og festeanordningene forventes å være godt overdimensjonerte, selv om de ikke ser slik ut.  Mao bør det være trygt å henge taklamper på de eksisterende taklampene.  Med store S-kroker fra Europris er det relativt enkelt å henge opp lamper på lampene.  Det er dermed ikke nødvendig å rigge mye for å arrangere taklampeutstilling - men det er helt nødvendig å stå på en stol mens man tar opp/ned lamper.

Vi brukte i oktober 2018 to av de monterte rektangulære taklampene til å henge opp de fleste taklampene vi hadde til salgs, og puttet to store klaffbord under.  Det er viktig at det står bord under taklampene, slik at folk ikke dunker hodene inn i lampene.

## Publikumskontroll

* Søndag september 2019 hadde vi i begynnelsen én person i trappa som regulerte innstrømming til både elektro og husgeråd.  Ulempen her er at det ble lite kunder på elektro og mye på husgeråd, men dette er egentlig ikke en opsjon av brannsikkerhetsårsaker.
* September 2019, med lamper på gangen og annet elektrisk på klasserommet, vurderte jeg det dithen at hvis vi ikke fikk nok bemanning kunne vi rett og slett låse av klasserommet og bare selge lamper i morgenrushet.
* Lørdag september 2019 mistet vi fullstendig kontrollen i det første kvarteret.  For søndagen vurderte jeg det slik at vi kunne ha en person til å låse døra dersom det kom for mange kunder inn på en gang.
* Minimumsbemanning ved lampeutstilling på gangen er vel to personer bak kassen (bør være tre), en person på innsiden av døra, og én person på utsiden av døra.

## Sikker sone

Høsten 2018 hadde vi stort sett kontroll bak disken, selv om enkelte surret seg inn på feil side av og til.  En stor hyllereol ble snudd og plassert øverst til venstre i lokalet, med litt plass mellom hyllereolen og vinduet, og bak disken.  Ledig hylleplass i hyllereolen kunne dermed brukes til private eiendeler til vaktene samt annet utstyr som vi ønsket å holde kontroll på.


