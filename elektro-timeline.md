Liste over gjøremål, elektro (ved bruk av klasserom sønnafor husgeråd)

## Fredag

### Forberedende arbeid

* Det er nødvendig å ha med seg penn og papir til klasserommet
* Ta masse bilder over hvordan klasserommet er møblert, fra forskjellige vinkler.
* Ta ut mesteparten av stolene fra klasserommet, sett de forran dør til grupperom på utsiden (la det være 5-10 stoler igjen i klasserommet - vær oppmerksom på at like stoler stables best)
* Lag et kart over hvordan pultene stod.  Evt, legg lapper inni pulten med posisjonsangivelse (klistremerker er en dårlig idé, kan være vanskelig å fjerne).
* Organiser pulter til salgsbenker.  Husk på at de må stå riktig vei, publikum må ikke kunne ta ut bøker o.l. fra pultene.
* Hent og rigg to store klaffbord til billigvarer under lampeutstilling
* Hent og rigg evt ett stort klaffbord til hygienevarer e.l.
* Hent og rigg to store klaffbord til lampeutstilling
* Legg A4-ark på pultene med beskrivelse av hva som skal hvor (slik at folk som sorterer varer kan legge ting riktig).  (bruk evt "lærertyggis")
* Marker en "drop zone" på utsiden av klasserommet (eller på et ledig bord rett innafor døra) for innkommende usorterte varer (ellers havner de fort sammen med sorterte ting til utstilling)
* Finn frem fem esker for søppel - elektrosøppel og vanlig søppel, to esker utenfor klasserom, to esker bak disken - pluss en eske for "ukjente deler" som står bak disken (ofte finner man plutselig ut hvilken gjenstand som mangler delen - av og til kommer kjøpere tilbake og spør etter deler som mangler).

### Organisering av salgsvarer, fredag og lørdags kveld

* Vinduskarmer - finn en passlig verdiløs kabel/ledning, legg den langs yttersiden av karmen og fest den der med vevtape - vips så har man en stoppekant slik at salgsobjekter kan plasseres litt på skrått uten å ramle på gulvet.
* Sortere varer.  Alt av kabler, verktøy, reservedeler, prislapper, lyspærer, skjøteledninger, etc, legges sentralt bak disken
* Finn kroker til taklampeutstilling, heng opp taklamper
* Hvis det blir ledig tid, teste ting (prioriter musikk), gjøre research på priser, evt legge ut varer på finn
* Sortere kabler, strømforsyninger, reservedeler, etc.
* Tomme poser legges i esker og stables pent langs veggen
* Dersom det er flere eksakt like gjenstander (f.eks. 2 stavmiksere av samme merke og modell) eller mange nesten like gjenstander (f.eks. 8 ulike stavmiksere), putt vekk duplikater i en eske og gjem bort til senere.
* Vi bør ha minst ett komplett musikkanlegg av noe slag som faktisk fungerer, og som kan brukes for å spille musikk på loppemarkedet.  Et fungerende, demonstrerbart anlegg er adskillig mer verd enn et kanskje-fungerende anlegg som ikke er testet.

## Før start, lørdag og søndag

* Vurdere om det skal brukes "solgt"-lapper og i såfall bli enig om hvilken standard som brukes.
* Se igjennom utstillingen og sørge for at den ser presentabel ut
* Ta med taperull, tau, saks, strips, blanke plastposer - se etter varer som har mange løse deler, de må sikres (evt legges bak disken).  Samme med nye gjenstander som er pent pakket inn i orginalemballasje
* Organisere skjøteledninger under pultradene
* Finn prislapper og penn, gå rundt og sett priser på ting.
* Teste ting (prioriter musikkanlegg), vurdere om ting skal være påskrudd for demonstrasjonseffekten.  Merk utstyr med "testet OK".  Finn ut hvilket utstyr som trenger batterier.  Gjenstander av lav verdi: ta ut batterier som fungerer.  Gjenstander av høyere verdi: se til at de har nok batterier.
* Sortere kabler, reservedeler, strømforsyninger, verktøy o.l. - vurdere hva som skal legges ut for salg og hva vi må beholde selv
* Diskutere hvem som står hvor og ansvarsområder
* Følg med på klokka - vekslepenger hentes 10:45, alle må ha gule vester, alle bør vurdere behov for dobesøk, etc.

## Ved ledige stunder

* Se over alle benkeradene og rydde opp.  Løse deler samles sammen og legges i esken.  Ting som har havnet på feil sted puttes tilbake.  Fyll på med tomme poser om nødvendig.
* Dersom man har ledig tid - teste lyspærer og batterier og se hvilke som fungerer
* Teste øvrig utstyr, lete etter priser, evt kaste ting som vurderes som usalgbare
* Legge ut annonser på Finn - særlig sære gjenstander som kan ha verdi for rette vedkomne, samt store/tunge ting man ønsker å bli kvitt.

## Søndag, avslutning

* Allerede før stengetid bør man begynne å vurdere hva man skal ta vare på til neste år.  Store/tunge gjenstander av verdi bør man prøve å få solgt - sett ned prisen og markedsfør gjenstandene aggressivt.  Kanskje legge ut på Finn?
* Ta vare på strømforsyninger, batterier, kabler og pærer (her kan man begynne allerede før stengetid).
* Vi bør ha et tilstrekkelig antall lampettledninger m/brytere, finn gode kabler på lamper og klipp av.
* Få tak i returtraller, sorter tas-vare-på, søppel og elektrisk avfall.  Enkelte store gjenstander bør deles hvis det ikke er for mye arbeid, f.eks. går lampeskjermer som ikke-elektrisk søppel, store veggur har ofte en bitteliten elektrisk klokke på baksiden, etc.
* Få ned riggutstyr, reservedeler, verktøy etc i en eller flere godt merkede esker.
* Få alt av salgsvarer, utstyr, etc ut av klasserommet.
* Vask vinduskarmer, pulter og andre overflater
* Vask gulvet
* Flytt pulter på plass
* Ta en rask omgang over gulvet igjen - blir fort striper der hvor pultene har vært flyttet
* Få inn stoler
* Lukk døra
* Få ut søppel
* Få ut elsøppel
* Fint om noen tar ansvar for å få kjørt bort elsøppel
* Vask gangen utenfor klasserommet
* Hjelp de andre avdelingene
