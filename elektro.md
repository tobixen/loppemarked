# "Manifest" for loppemarked, avdeling elektro.

Se også [gjøremålsliste](elektro-timeline.md)

## Publikumskontroll

### Problem"kunder"

Følgende typer "kunder" har vi problemer med hvert år - og det kan tære veldig på humøret vårt, så det er viktig å ha strategier klare for å møte disse.

* Tyver.  Ja, de finnes.  Noen tenker kanskje at det er greit å stjele fordi vi ikke har nok kapasitet til å betjene alle kundene.  Noen har manglende forståelse eller kunnskap om konseptet med loppemarkedet som et veldedighetsprosjekt, noen tenker at det ikke er noen stor sak fordi vi allikevel har fått varene gratis.  Og ja, de utgjør et problem, fordi det typisk er de mest attraktive varene som blir stjålet først (og det gjør også noe med humøret vårt).  Tiltak:
  * Den mest effektive måten å begrense svinn på er å putte de varene som har høyest verdi til utstilling bak disken, slik at publikum eksplisitt må be om å få ta en ekstra titt.  Dette innebærer imidlertid også mindre omsetning og mer tidsbruk.
  * Kort avstand fra vakter til loppene antas å ha en effekt, selv om vi har opplevd tyveri fra "disk" rett forran nesen på vakter
  * Å ha et godt forhold på antall vakter vs publikum slik at man kan holde litt øye med folk er en av de tingene som fungerer best, men ikke alltid mulig
  * Vi har forsøkt å bruke tau for å binde de mer verdifulle varene fast.  Alle vaktene skulle da ha en saks i pengevesken slik at de raskt kunne klippe ting fritt ved behov.  Dette hadde nok litt effekt, men det innebar også mye trøbbel ettersom saksene fort ble glemt igjen på benkene og havnet på avveie - og dedikerte tyver vet å få tak i en saks.
  * Bruk av "solgt"-lapper og en solid og streng dørvakt som sjekker hver eneste person som går ut nøye - dette har vi konkret sett at har hatt effekt, ved at enkelte har blitt tatt med varer de ikke har betalt for.  Det er imidlertid umulig å gardere seg mot tyveri på denne måten, og det forutsetter også at alle vaktene har tilstrekkelig med solgt-lapper på seg eller ved seg til enhver tid (og at de ikke havner på avveie).
  * Korpsbarn uten refleksvest som raskt har rapportert til en voksen har ved ett tilfelle ført til at en tyv ble tatt på fersk gjerning (og vi økte omsetningen med kr 5 for kabelen som ble forsøkt stjålet).
* "Samlere" som plukker med seg et lass med varer fra utstillingen, og deretter ønsker å kjøpe hele lasset til spottpris.  Dette medfører ofte at varene som er pent og pyntlig sortert og stilt ut havner på helt gale steder.  Jeg pleier å være ganske stram på at dersom en kunde plukker opp en vare, så skal den betales for med det samme.  En fin ting kan være å fysisk ta varen fra kunden og putte den på utstilling bak disken.
* Prutere.  Det er etter min mening helt greit å forsøke å prute på varer, det kan være nyttig for å få solgt varer vi har priset alt for høyt.  Det er imidlertid et stort problem med de som ikke kan ta et "nei" for et "nei", og som legger mye beslag på tiden vår mens det er mange kunder eller mye kø.  Å fysisk ta varen fra kunden, plassere den på utstilling bak disken for deretter å vie oppmerksomheten til en annen kunde kan hjelpe.  Jeg pleier også å be pruterene å komme tilbake senere, hvis varen ikke er solgt før kl 14 kan de få billigere pris.  Dette kan gjerne suppleres med at man klasker på en ny prislapp, fra "100" til "100 NÅ | 50 etter kl 14:00".  "Du behøver jo ikke kjøpe denne varen" er en fin replikk.
* "Holdere".  De finner en eller flere varer de har lyst på, og så knuger de på den.  Av og til foregår det hektisk mobilaktivitet for å få aksept fra familiemedlemmer eller finne ut om varen er verd noe, andre ganger pga et løfte om "50 kroner etter kl 14:00", atter andre gjør det for at vi skal gå lei og godta prutingen.  En mulig innfallsvinkel er å ta varen fra kunden og plassere den på utstilling bak disken.
* "Rotere" - de løfter opp ting og putter de ned igjen på andre steder, dersom det er en eske med noe i, åpner de eska, tømmer den, og pakker ikke ting skikkelig på plass igjen (selv om det er bilder på utsiden av boksen over hva som er i, selv om de ikke har bruk for en slik ting, og selv om prisen på eska er utenfor budsjett).  Det mest effektive er å gripe tak i dem tidlig og kreve penger for tingene de rører.  "Betal først, åpne etterpå, og penger i retur hvis du er saklig misfornøyd" fungerer av og til.
* Andre tidstyver - de vil ha varene demonstrert og testet, selv om de ikke har planer om å kjøpe de (evt, de tilbyr en femmer når varen er ferdig demonstrert).  "Betal først, teste etterpå, og pengene tilbake om det ikke virker" er en god innfallsvinkel.
* Priskuttere.  De gjør hva de kan for å få kjøpt ting billig.  Dersom det ikke nyttet å prute med én vakt, så griper de tak i en annen.  Dersom loppen har en prislapp med en uakseptabelt høy pris, så rives den av før prutingen begynner.  Dersom loppen er ny og innpakket i eske med bruksanvisning, så tas den ut av esken før prutingen begynner, slik at gjenstanden ser brukt ut.  Etc.  Alle disse triksene er ufine, og enkelte er helt uakseptable.  Man burde nesten vurdere svartelisting og utkasting av enkelte gjengangere.

Problemkundene kommer typisk rett etter åpningstid.  Noen av punktene over er jo også svært problematisk når man har mye pågang og lang kø, men fullt ut akseptabel når det ikke er kunder i det hele tatt.

### Begrensinger på kundeinnstrømmingen

Det er etter min mening en stor fordel å ha noen som sitter i døra og fører stram kontroll på hvor mye publikum som får lov å komme inn i lokalet i begynnelsen av loppemarkedet.  For mange kunder inne i lokalet medfører stort svinn, mye rot, mye stress, større risiko ifbm brann og dårlig humør for oss som jobber der.  På den andre siden, slipper man inn for få på en gang blir det lang kø, noe som også er utrolig upopulært (og som også kan være problematisk dersom det akkumuleres kø på steder det ikke bør være kø).

Kølapper kunne kanskje fungert, en ubetinget fordel da er at folk kan besøke de andre avdelingene eller kaféen mens de venter på tur.  Vi gjorde et forsøk på dette søndag 28. oktober 2018, men det ble meget kortvarig da det ikke var like stor pågang av kunder som ellers.  Førsteinntrykket i den korte perioden vi hadde kølappsystem var også at folk var negativt innstilt til ordningen.  Jeg tror imidlertid idéen var god og bør testes ut igjen neste gang vi har problemer med for mange kunder.  Slik var tanken at det skulle fungere:

* Tre plakater over hverandre ved døra, "Nå slipper vi inn de med kølapp", "0", "og lavere", hvor den midterste plakaten byttes ut med 10, 20, 30, etc etterhvert som vi kan tillate flere kunder.
* Dørvakta deler ut kølapper til de som vil ha (dispenserautomat hadde vært en fordel)
* Dørvakta samler inn kølappene når folk skal inn i lokalet.  Dersom noen skal ut og komme tilbake igjen blir det ny kølapp.  Aller helst burde vi også hatt en ordning for å få kastet ut folk som blir værende for lenge i lokalet uten å kjøpe noe.

## Lokasjon

Vi har hatt loppemarked tre steder ila de siste årene, klasserommet sønnafor husgeråd (nært utendørs trapp), klasserommet norrafor husgeråd (nært lærerværelse) og samlingssalen.  Pr 2018/2019 har vi havnet sønnafor hugeråd.

Dersom man har mange lopper blir det for trangt på klasserommene, da er samlingssalen mer idéell - men med god planlegging og evt også bruk av arealene på utsiden av klasserommet for utstillinger og salg, så fungerer klasserommet bedre.

Detaljert beskrivelse av hvordan ting ble gjort finnes i egne dokumenter:

* [Samlingssalen](elektro-i-samlingssal.md)
* [Klasserommet](elektro-i-klasserom.md)

## Prising av varer

### Klassiske problemstillinger

Ønsker vi å bli kvitt mest mulig, eller ønsker vi å få høyest mulig priser?  Ønsker vi å ha alt på utstilling fra starten av, eller ønsker vi å sette ut nye ting etterhvert?  Er "posesalg" greit, og er det greit at "oppkjøpere" kjøper store kvanta med restvarer på slutten av søndagen?  Her er det mange personlige meninger, og det gis også ofte signaler fra loppeledelsen.  I de siste årene har signalene gått på at prisnivået skal holdes høyt, at vi tar vare på mest mulig, at det er bedre å kaste noe enn å selge til underpris (bl.a. pga signaleffekt).  På elektro har vi allikevel kjørt en linje med at vi ønsker å kvitte oss med mest mulig. Noen ting å tenke på:

* På elektro har vi generelt hatt praksis med å stille ut alle varene fra starten av, og ofte får vi solgt det aller meste ila dagen.  Under dette regimet er det helt greit å gjøre folk oppmerksomme på at de kan komme tilbake senere dersom de synes prisen er for høy - dersom kunden virkelig ønsker en ting, så vil de heller betale full pris enn å risikere at varen blir kjøpt av noen andre.
* Mange elektroprodukter synker i verdi fra loppemarked til loppemarked, teknologier og standarder fra i forgårs er mindre verd enn teknologier og standarder fra i går.  Slik sett har vi på elektro et litt større press på å få varer ut av døra enn de andre avdelingene.
* Nå (2018/2019) betaler vi ikke for å kaste avfall - men for elektro innebærer returen arbeid for noen, det er ofte mer ønskelig å få en vare billig ut døra søndagen sammenlignet med å måtte kaste.
* Ofte forsvinner det aller meste av varer i løpet av den første timen - i såfall har vi kanskje priset ting for lavt, eller vi er for løsslupne på prutingen.
* Særlig på lamper er det kanskje viktig å ha et størst mulig synlig utvalg, kundene kjøper de lampene de har lyst på, har vi ikke rett lampe til rett person så blir det ikke noe salg.  Her er også folk interessert i å kjøpe f.eks. to like lamper.
* For gjenstander det er naturlig å eie én av, bør vi gjemme bort duplikatvarer (samme merke og modell, men kanskje litt forskjellig tilstand) samt gjemme bort varer dersom vi har for mange av ett slag (f.eks. 4 stavmiksere av litt forskjellig modell).  Ved å ha for mye likt fremme, risikerer man uryddig presentasjon (pga for lite plass), at alle stavmikserene forsvinner ila første kvarteret (da har man antageligvis priset de for lavt, eller ikke vært godt nok på vakt mot tyver), eller irritasjonsmoment med at kunden ønsker å teste alle fire stavmikserene.  Utover dagen (særlig søndagen) bør man vurdere å vise alle - da er det greit om noen kjøper alle, det er større sjanse for salg når kunden har flere modeller/tilstander å velge mellom, mer tid tilgjengelig for hver enkelt kunde.
* Jo mer som blir solgt, jo raskere går ryddearbeidet på slutten av dagen.

### Generelt

Prising er en utfordring.  Under paradigmet at vi kan ta vare på lopper til neste loppemarked er det generelt bedre å sette for høye priser enn for lave.  For de gjenstandene man antar at har verdi bør man gjøre litt søk på finn.no, evt ebay.no eller andre nasjonale eller internasjonale nettbutikker for å få et hint om hva markedspris ligger på.  Det er også klart at dersom vi f.eks. har fem tastaturer, så bør vi vurere å prise de lavere enn dersom vi bare har ett tastatur (vært på vakt dersom noen ønsker å kjøpe *alle* tastaturene, det er et tegn på at prisen er for lav, si at de må betale mer for den siste eller de to siste - evt, legg bare ut et par tastaturer på utstilling og la tre ligge bak kassa i reserve).

Det er mye som selges nytt for såpass lave priser at man vanskelig kan konkurrere, f.eks. kan man ofte finne vannkokere og panelovner for så lite som en hundrelapp på Biltema eller Elkjøp.  Man må være oppmerksom på at anerkjente merker kan gi en god merverdi.  Det er en fordel at flest mulig vakter tar en titt over sortimentet og peker ut det de tror har høyest verdi.

Argumentet "jeg vet jo ikke om den virker" blir ofte brukt for å be om en lavere pris.  Dersom man antar at en vare fungerer, tilby gjerne en "ut-døra-garanti" eller "ut-dagen-garanti" og be folk om å teste ut varer *etter* at de er betalt for.  Dersom det er mye pågang er det lite vits i å bruke masse tid på å teste et produkt som kunden allikevel ikke har planer om å betale full pris for.  Når ting er testet, så klistre gjerne på en merkelapp med "testet OK".

"Dutch auction": på starten av dagen forholder vi oss strengt til prislappene på varene, utover dagen går vi ned i pris.  Vanlig auksjon: Dersom en vare er priset til 100 og en kunde sier at de vil kjøpe den for 50, så kan vi konkludere med at den er verd minst 50.  Klistre på en lapp med "bud: 50" slik at vi husker at varen uansett ikke skal selges for under 50 kroner.

### Symaskiner, støvsugere, strykjern

Symaskiner, støvsugere og strykjern blir ofte revet vekk fortere enn hva vi får dem inn, dette er ting folk spør etter.  Ikke selg disse tingene for tidlig.  Dersom noen byr lavere enn annonsert pris så noter det som et "bud".

Symaskiner - her har vi vanligvis manglet kompetanse, men følgende tommelfingerregel ser ut til å fungere greit: uansett hva slags modell, tilstand eller årgang, start på 300 kroner.  Prisen kan settes ned til 200 kroner etter lunch lørdag, 150 kroner søndags morgen og 100 kroner søndag etter lunch.  Ta i mot bud.  Lytt til kunder som forteller hva slags verdi de tror den har, hva som er galt med symaskinen eller hvorfor den burde gå billigere, men ikke la symaskiner gå til under 300 kroner lørdag før lunch, uansett - såfremt den ikke er åpenbart i dårlig stand eller dersom vi har mange symaskiner.

Nysalg av støvsugere med høy effekt er i ferd med å bli forbudt i EU - alt av støvsugere med høy effekt kan derfor være av verdi.  Små, gamle batteristøvsugere er stort sett helt verdiløst da batteriet er defekt og ikke følger noen standard.  (ta evt vare på strømforsyningen).

Et presentabelt strykjern kan man forsøke å få 100 kroner for.

### Kjøkkenelektrisk

Kjøkkenelektriske artikler blir generelt revet vekk fortere enn hva vi får dem inn.  Startpris på en rene og pen vannkoker kan gjerne settes til en hundrelapp eller mer - dersom vi fortsatt har flere vannkokere igjen mot slutten av dagen kan prisen justeres ned til 30-40 kroner, men ikke lavere.  En godt sliten vannkoker får man neppe solgt for mer enn 30 kroner.

Kaffetraktere, det var to loppemarked da "alle" ville kvitte seg med kaffetrakterene sine og kjøpe kapselmaskiner eller espressomaskiner - da var disse tungsolgt, men denne perioden er over.  Kaffetraktere er verd mellom 10 kroner og 200 kroner avhengig av modell og tilstand.  "Moccamaster" er generelt et ettertraktet merke.

Startpris på en fungerende food processor m/ tilbehør kan godt settes til 150-200 kroner.  Prisen kan lempes noe mot slutten av dagen, men ikke under 50 kroner.  Deler til foodprocessoren forsvinner fort - en mulighet er å putte tilbehør i en gjennomsiktig pose som tapes/knytes godt til maskinen, eller sette alt på utstilling bak kassen.  Publikum som på eget initiativ begynner å åpne posen bør jages ut av lokalet, men dersom det er lite pågang kan man gjerne hjelpe til med å demonstrere at alle delene fungerer som tiltenkt.  Dersom man ikke har kapasitet til å passe på foodprocessors, så putt de "trygt" bak disken.  Gi "penger-tilbake-garanti" og la kunden se/prøve produktet /etter/ at det er betalt for.

Startpris på en fungerende brødbakemaskin med fin eltekrok kan også godt settes til 200 kroner, og bør ikke lempes til under 80 kroner.

Et fint, moderne og lite brukt vaffeljern kan prises til 80 kroner og selges for 30 kroner mot slutten av dagen.  Vaffeljern som ikke ser særlig pene ut kan prises til 20-30 kroner.  Samme gjelder for toastjern.  En fin, moderne og lite brukt brødrister kan prises til 80 kroner.

### Hygiene- og helseartikler

Krølltanger, massasjeting, hårfønere, varmetepper, blodtrykkmålere, fotbad m/ massasjeelementer, elektriske negelfiler, etc.

Man skulle tro at mye av dette ikke ble solgt da det er ekkelt eller uhygienisk å ta over noe som andre har brukt, men erfaringsmessig blir det meste solgt dersom prisen er riktig.

Startpris på en fin hårføner kan gjerne settes til en hundrelapp.  Fotbadene får vi ofte fortere inn enn hva de går ut.

### Klokker

Vi får ofte inn mye klokker (veggklokker, gamle armbåndsur, vekkerklokker, stålamper, etc), de selges på elektro uansett om de er batteridrevne eller mekaniske.  Gamle bestefarsklokker og gjøkur kan ha noe verdi (selv om de er helt eller delvis defekte), et armbåndsur av riktig merke kan være av svært høy verdi - men de fleste vi får inn er neppe verd mer enn en tikroning eller to.  Klokkene kan ofte være vanskelig å bli kvitt, de selges nok i snitt for under 20 kroner stykket, men det er jo mulig å prøve seg med litt høyere priser på begynnelsen av dagen, 40 eller 50 er et fint tall.  Husk på å ta betalt for batteriene, dersom man setter inn nye batteri.

### Verktøy

Verktøy selges normalt på elektro, uansett om det er elektrisk eller ikke.

Generelt - ikke selg verktøy for billig - og legg gjerne litt tilside, det er stadig behov for verktøy ifbm reperasjon/testing av lopper.

### Kabler, skjøteledninger, lyspærer, batterier, strømforsyninger og reservedeler

Noen ting man bør tenke på:

* Vi *trenger* disse greiene for å teste ting, så man bør vurdere om mye av dette i det hele tatt skal legges ut for salg.  Det er kjipt dersom man plutselig trenger en helt standard USB-mikro-kabel eller en helt standard strømkabel og det ikke finnes tilgjengelig.

* Vi kan få adskillig merpris for gjenstander (evt, få de solgt i det hele tatt) dersom vi kan supplere ting med en passende kabel, strømforsyning, batteri eller lyspære.

* Det som akkumulerer seg har sjeldent noe høy verdi, og bør selges billigst mulig.  Samme dersom vi har for mange like kabler av samme sort, så bør vi prøve å få dem solgt (men ikke nødvendigvis billigst mulig, og aldri selg alle).

* Det blir fort mye rot dersom kunder selv får lete igjennom kabelsamlingen, og det blir også fort slik at alt som har verdi forsvinner.  Ofte uten at folk gidder betale engang.

Fra og med søndag 28. oktober 2018 har vi kjørt med følgende løsning på kabler og strømforsyninger:

* Alle kabler og småting som jeg definerte som "relativt verdiløst" blir lagt ut i esker slik at kunder kunne rote igjennom og plukke ut ting (f.eks. til fem kroner stykket eller 20 kroner for hele eska).  Tekst om at vi har flere strømforsyninger og kabler bak disken.

* Alle strømforsyningene, samt alt av kabler jeg anså at kunne ha verdi for oss sortert i mindre esker/bokser og ble liggende bak disken.  Dette ble solgt til litt høyere pris (typisk 10-20 kroner).  Dette må aldri settes frem slik at kundene kan rote i dem på eget initiativ, og vi må alltid være tilbakeholdne på å selge kabler vi kan få bruk for.

Dette synes jeg fungerer ganske tilfredsstillende.  Det er noen få som er på utkikk etter en bestemt kabel eller strømforsyning, på spørsmål kan vi da raskt finne frem riktig kabel.  Andre trenger kabler ifbm kjøp av lopper som mangler slikt - igjen, vi fant raskt frem riktig kabel.  De som bare ville kikke og lete etter underprisede varer får rote rundt på egen hånd, men ikke i sorterte bokser med kabler av antatt verdi.

Batterier kjøper vi inn nytt.  Husk på å få tilstrekkelig betalt for gjenstander med batterier.  Hvis folk vil prute, så ta ut batterier og lyspærer før ting selges til underpris.

Lyspærer må ikke være direkte tilgjengelig for kundene.  De bør sorteres, og dersom vi har for mange av én sort kan vi legge ut for salg.  Gamle glødelamper koster svært lite i produksjon, men de er i ferd med å fases ut og kan derfor være verdifulle!

Skjøteledninger til 220V, særlig lange kabler og koblingsbokser kan ha litt verdi, man bør neppe selge det til under halvparten av nypris.  Forgrenerskjøteledninger bør ikke selges i det hele tatt såfremt vi ikke har riktig mange av de.

### Stereo/video etc

Ting som er retro kan være av høy verdi, særlig Tandberg-utstyr.  Ikke selg slikt uten å gjøre research på prisen.  Av og til kan man få en tusenlapp eller to for noe "gammelt ræl".  Viktig at alle er oppmerksom på dette, vi har ofte sett at folk har fått prutet ned prisen til en brøkdel av prislapp (evt, at varen ble solgt til en slikk og ingenting før prislappen kom på) tidlig lørdag.

FM/AM-radioer er ganske ut - men ikke helt!  Det er fortsatt nærradiosendinger på FM-båndet, og man skal ikke lengre enn Sverige før FM-radioen har en bruksverdi.  Det er stort sett ikke noe vi ønsker å ta vare på fra ett loppemarked til et annet, så kan godt selges til godt nedprutet pris mot slutten av dagen.

CD-spillere er også ganske ut.

Et komplett lydanlegg med høyttalere kan kanskje selges for mellom 50 og 300 eller mer avhengig av kvalitet, merke, effekt, kapasitet, etc.  Et musikkanlegg som bare tar standard audioCD'er pluss radio får man neppe særlig mye for - men det er lov å prøve, og et anlegg som faktisk spiller musikk er mer salgbart enn løse komponenter som kanskje kan spille musikk.  Musikkanlegg har også en bruksverdi for oss, alle avdelingene setter pris på musikk til arbeidet.  Dersom vi har ett eller to anlegg igjen som faktisk fungerer godt nok til å fylle et klasserom med musikk, så ikke selg for under 150.

VHF-spillere bør vi hverken selge for billig eller kaste.  Selv om VHF-formatet er "dødt", så finnes det en betydelig mengde VHF-taper der ute - inkludert hjemmeopptak - som folk ønsker å beholde og/eller digitalisere.  For å få til dette kan de trenge en spiller.

Platespillere bør vi generelt ikke kaste - men det er verd å merke seg at brorparten av verdien i en platespiller kan ligge i stiften.  En platespiller uten stift kan være usalgbar, avhengig av modell.

### Datautstyr

Datautstyr som vi får inn er ofte såpass gammelt at det er verdiløst allerede i det vi får det inn.

Dataskjermer eller TV'er uten relativt moderne tilkoblingsstandarder (HDMI er minimum pr 2018/2019) kan godt prises under 50 kroner.

### Verktøy

Ikke selg verktøy for billig! Vurder om vi trenger verktøyet selv.

## Rigge/salgsutstyr

* S-kroker, evt tau til å fikse taklampeutstilling
* Koblingsklemmer, f.eks. https://www.clasohlson.com/no/Wago-tilkoblingsklemmer-/36-4863 for å koble på stikkontakter eller strømplugger på utstyr som mangler slikt
* Verktøy for å fikse ting
  * sett med småjern
  * litt større flatskrujern
  * Avbitertang
  * Multimeter (mangler pr september 2019)
  * Avisoleringsverktøy
* Batterier - mellomstore 1.5v (C?), AA, AAA, 9V, andre
* Strips - til å stripse sammen ledninger (lite igjen)
* Merkelapper/prislapper
* Solgt-lapper
* Sakser
* Lyspærer av forskjellige størrelser (ta vare på disse!  Når vi får "for mange", så selg enkeltvis eller i en hel kasse).
* Reservedeler til lamper: skruting for lampeskjerm, bordfeste for skrivebordslamper, etc
* Kabler og strømforsyninger, sortert i mindre esker/bokser
* sticky pad

## Annet

Viktig å ha en klart definert "sikker sone", hvor publikum ikke skal ha noen som helst adgang, og hvor vi kan legge ting (f.eks. egne jakker, ryggsekker, ting som vi selv har kjøpt, evt også ting publikum har betalt for og vil at vi skal legge til side).
